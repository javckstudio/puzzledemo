-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

--=======================================================================================
--引入各種函式庫
--=======================================================================================
display.setStatusBar( display.HiddenStatusBar )

--=======================================================================================
--宣告各種變數
--=======================================================================================
_SCREEN = {
	WIDTH = display.viewableContentWidth,
	HEIGHT = display.viewableContentHeight
}
_SCREEN.CENTER = {
	X = display.contentCenterX,
	Y = display.contentCenterY
}
local background
local targetPic
local allDone
local pic1
local pic2
local pic3
local pic4
local youWin
local isIhpone5 = false
local rightNumber = 0
local buttonPressedSound = audio.loadSound( "ButtonPressed.mp3")
local magicSound = audio.loadSound( "Magic.mp3" )
local putDownSound = audio.loadSound( "PutDown.mp3")
local puzzleTouch
local playAgain
local winTheGame
local checkPosition
local checkOutIfIsIphone5
local makeBackground
local reset
local chkFlag = false
local sensitive = 27

--=======================================================================================
--宣告與定義main()函式
--=======================================================================================
local main = function (  )
	checkOutIfIsIphone5()
	makeBackground()
end

--=======================================================================================
--定義其他函式
--=======================================================================================
makeBackground = function ( )
	if (isIhpone5) then
		background = display.newImageRect( "BackgroundiPhone5.png", 640, 1136 )
	else
		background = display.newImageRect( "Background.png", 640, 960 )
		targetPic = display.newImageRect( "TargetPic.png", 375, 274 )
		targetPic.x = 320
		targetPic.y = 484
	end
	background.x = _SCREEN.CENTER.X
	background.y = _SCREEN.CENTER.Y

	

	allDone = display.newImageRect(  "AllDone.png", 372, 373 )
	allDone.x = 319
	allDone.y = 483
	

	pic1 = display.newImageRect( "1.png", 192 , 263 )
	--自訂屬性策略
	pic1.startPosition = {x=524,y=756}
	pic1.rightPosition = {x=228,y=422}

	pic2 = display.newImageRect( "2.png", 262 , 192 )
	pic2.startPosition = {x=118,y=738}
	pic2.rightPosition = {x=377,y=387}

	pic3 = display.newImageRect( "3.png", 262 , 192 )
	pic3.startPosition = {x=509,y=282}
	pic3.rightPosition = {x=263,y=576}

	pic4 = display.newImageRect( "4.png", 193 , 261 )
	pic4.startPosition = {x=73,y=310}
	pic4.rightPosition = {x=411,y=541}

	youWin = display.newImageRect( "YouWin.png", 640, 225 )
	youWin.x = _SCREEN.CENTER.X
	youWin.y = _SCREEN.HEIGHT - 112.5

	reset()

end
--建立reset function來將舞台還原
reset = function ( )
	allDone.isVisible = false
	pic1.x = pic1.startPosition.x
	pic1.y = pic1.startPosition.y
	pic1.isVisible = true
	pic2.x = pic2.startPosition.x
	pic2.y = pic2.startPosition.y
	pic2.isVisible = true
	pic3.x = pic3.startPosition.x
	pic3.y = pic3.startPosition.y
	pic3.isVisible = true
	pic4.x = pic4.startPosition.x
	pic4.y = pic4.startPosition.y
	pic4.isVisible = true

	youWin.isVisible = false

	pic1:addEventListener( "touch", puzzleTouch )
	pic2:addEventListener( "touch", puzzleTouch )
	pic3:addEventListener( "touch", puzzleTouch )
	pic4:addEventListener( "touch", puzzleTouch )
end

puzzleTouch = function ( event )
	if "began" == event.phase then
		audio.play( buttonPressedSound )
		event.target:toFront( )
		event.target.xScale = 1.1
		event.target.yScale = 1.1
		--只讓這個元件接收event
		display.getCurrentStage( ):setFocus( event.target)
		event.target.isFocus = true
	elseif "moved" == event.phase then
		event.target.x = event.x
		event.target.y = event.y
	elseif "canceled" == event.phase or "ended" == event.phase then
		event.target.xScale = 1
		event.target.yScale = 1
		--重新讓所有元件都能收到事件通知
		display.getCurrentStage( ):setFocus( nil)
		event.target.isFocus = false
		checkPosition(event.target)
	end
end

checkPosition = function ( thePic )
	chkFlag = false
	--math.abs為取絕對值，用與正確位置的差值來比較是否在範圍內
	if (math.abs(thePic.x - thePic.rightPosition.x) < sensitive ) then
		if (math.abs(thePic.y - thePic.rightPosition.y) < sensitive ) then
			chkFlag = true
			thePic.x = thePic.rightPosition.x
			thePic.y = thePic.rightPosition.y
			thePic:removeEventListener( "touch", puzzleTouch )
			rightNumber = rightNumber + 1
			if(rightNumber == 4) then
				winTheGame()
				rightNumber = 0
			else 
				audio.play( putDownSound )
			end
			print( "Position OK" )
		end
	end
	if (chkFlag == false) then
		print( "Position Wrong" )
	end
end

winTheGame = function (  )
	audio.play( magicSound )
	pic1.isVisible,pic2.isVisible,pic3.isVisible,pic4.isVisible = false,false,false,false
	allDone.isVisible = true
	youWin.isVisible = true
	local nextStep = function (  )
		Runtime:addEventListener( "touch", playAgain )
	end
	timer.performWithDelay( 500, nextStep )

end

playAgain = function ( event )
	if ("began" == event.phase) then
		audio.play( buttonPressedSound )
	else
		reset()
		Runtime:removeEventListener( "touch", playAgain )
	end
	
end
--檢查裝置是否為iPhone5或之後的版本
checkOutIfIsIphone5 = function ( )
	if (system.getInfo("model") == "iPhone" ) or (system.getInfo("model") == "iPod touch") then
		isIhpone5 = (display.viewableContentHeight > 960)
	end
end
--=======================================================================================
--呼叫主函式
--=======================================================================================
main()